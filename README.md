# devopscamp - adidas OPS challenge deployment

### Challenge
Your Mission, should you choose to accept it, is to create a deployment setup.

### Specifications
- Setup shall deploy a web application in CI/CD fashion
- During deployment it is mandatory to have no impact/downtime to service
- full deployment cycle < 5 min
- Deployment events shall be triggered to "devopscampadidas@gmail.com"
- any kind of deployment tool can be used

#### Optional
- Configs to be stored in this gitlab project
- Application can scale in realtime on demand
- rollback to version before 


